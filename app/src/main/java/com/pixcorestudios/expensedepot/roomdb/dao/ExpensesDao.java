package com.pixcorestudios.expensedepot.roomdb.dao;


import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.pixcorestudios.expensedepot.roomdb.models.ExpenseEntity;

import java.util.List;

@Dao
public interface ExpensesDao {

    @Insert
    public void addExpense(ExpenseEntity expense);

    @Update
    public void updateExpense(ExpenseEntity expense);

    @Query("SELECT * from expenses")
    LiveData<List<ExpenseEntity>> getExpenses();

    @Query("SELECT * from expenses WHERE date >= :start AND date <= :end")
    LiveData<List<ExpenseEntity>> getExpensesForDate(long start, long end);

    @Query("SELECT * from expenses where id = :exid")
    LiveData<ExpenseEntity> getExpense(int exid);


    @Query("DELETE from expenses Where id = :delid")
    void deleteExpense(int delid);



}
