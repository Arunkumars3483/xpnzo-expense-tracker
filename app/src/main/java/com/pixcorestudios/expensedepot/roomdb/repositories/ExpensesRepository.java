package com.pixcorestudios.expensedepot.roomdb.repositories;

import android.app.Application;

import androidx.lifecycle.LiveData;

import com.pixcorestudios.expensedepot.roomdb.dao.ExpensesDao;
import com.pixcorestudios.expensedepot.roomdb.database.ExpensesDatabase;
import com.pixcorestudios.expensedepot.roomdb.models.ExpenseEntity;

import java.util.List;

public class ExpensesRepository {

    private ExpensesDao expenseDao;
    ExpensesDatabase db;
    private LiveData<List<ExpenseEntity>> expenses;
    LiveData<ExpenseEntity> expense;

    public ExpensesRepository(Application application){
        db = ExpensesDatabase.getDatabase(application);
        expenseDao = db.expensesDao();

    }

    public LiveData<List<ExpenseEntity>> getAllExpenses(){
        expenses = expenseDao.getExpenses();
        return expenses;
    }
    public LiveData<List<ExpenseEntity>> getAllExpensesForDate(long start, long end){
        expenses = expenseDao.getExpensesForDate(start, end);
        return expenses;
    }





    public void insert(final ExpenseEntity expense){
        ExpensesDatabase.databaseWriteExecutor.execute(new Runnable() {
            @Override
            public void run() {
                expenseDao.addExpense(expense);
            }
        });
    }

    public void update(final ExpenseEntity expenseEntity){
        ExpensesDatabase.databaseWriteExecutor.execute(new Runnable() {
            @Override
            public void run() {
                expenseDao.updateExpense(expenseEntity);
            }
        });
    }

    public void delete(final int id){
        ExpensesDatabase.databaseWriteExecutor.execute(new Runnable() {
            @Override
            public void run() {
                expenseDao.deleteExpense(id);
            }
        });
    }
}

