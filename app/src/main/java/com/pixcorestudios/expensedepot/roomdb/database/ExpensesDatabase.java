package com.pixcorestudios.expensedepot.roomdb.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.pixcorestudios.expensedepot.roomdb.dao.ExpensesDao;
import com.pixcorestudios.expensedepot.roomdb.models.ExpenseEntity;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


@Database(entities = {ExpenseEntity.class}, version = 1, exportSchema = false)
public abstract class ExpensesDatabase extends RoomDatabase {

    public abstract ExpensesDao expensesDao();

    private static volatile ExpensesDatabase userDatabase;
    private static final int NUMBER_OF_THREADS = 4;
    public static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(NUMBER_OF_THREADS);
    public static ExpensesDatabase getDatabase(final Context context){
        if(userDatabase == null){
            synchronized (ExpensesDatabase.class){
                if(userDatabase == null){
                    userDatabase = Room.databaseBuilder(context.getApplicationContext(), ExpensesDatabase.class, "ExpensesDatabase").build();
                }
            }
        }
        return userDatabase;
    }

}
