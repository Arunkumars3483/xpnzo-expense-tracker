package com.pixcorestudios.expensedepot.roomdb.models;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

@Entity(tableName = "expenses")
public class ExpenseEntity {

    @PrimaryKey(autoGenerate = true)
    int id = 0;

    @ColumnInfo(name = "purpose")
    String purpose = "";

    @ColumnInfo(name = "date")
    long date;

    @ColumnInfo(name = "amount")
    long amount;

    public ExpenseEntity(String purpose, long date, long amount) {
        this.purpose = purpose;
        this.date = date;
        this.amount = amount;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }



    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }



    @Override
    public String toString() {
        return this.purpose;
    }
}