package com.pixcorestudios.expensedepot.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;


import com.pixcorestudios.expensedepot.R;
import com.pixcorestudios.expensedepot.roomdb.models.ExpenseEntity;
import com.pixcorestudios.expensedepot.utils.Utilities;

import java.util.ArrayList;

public class ExpensesListAdapter extends RecyclerView.Adapter<ExpensesListAdapter.ViewHolder>{
    private ArrayList<ExpenseEntity> expenses;
    private View.OnClickListener onDeleteItemClickListener;
    private View.OnClickListener onEditItemClickListener;
    Context context;

    public ExpensesListAdapter(ArrayList<ExpenseEntity> expenses, Context context) {
        this.expenses = expenses;
        this.context = context;
    }

    public void setDeleteItemClickListener(View.OnClickListener clickListener) {
        onDeleteItemClickListener = clickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.expenses_list_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ExpenseEntity expenseEntity = expenses.get(position);


        holder.amountTextView.setText("₹ " + expenseEntity.getAmount());
        holder.purposeTextView.setText("₹ " + expenseEntity.getPurpose());
        holder.dateTextView.setText(Utilities.getFormattedDateString(expenseEntity.getDate()));
        holder.actionsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                        //Creating the instance of PopupMenu
                        PopupMenu popup = new PopupMenu(context, holder.actionsButton);
                        //Inflating the Popup using xml file
                        popup.getMenuInflater()
                                .inflate(R.menu.popup_menu, popup.getMenu());

                        //registering popup with OnMenuItemClickListener
                        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                            public boolean onMenuItemClick(MenuItem item) {
                                if(item.getItemId() == R.id.edit_item){
                                    onEditItemClickListener.onClick(holder.itemView);
                                }else if(item.getItemId() == R.id.delete_item){
                                    onDeleteItemClickListener.onClick(holder.itemView);
                                }
                                return true;
                            }
                        });

                        popup.show(); //showing popup menu
            }
        });

//        holder.itemView.setOnClickListener(onItemClickListener);


    }


    @Override
    public int getItemCount() {
        return expenses.size();
    }

    public View.OnClickListener getOnEditItemClickListener() {
        return onEditItemClickListener;
    }

    public void setOnEditItemClickListener(View.OnClickListener onEditItemClickListener) {
        this.onEditItemClickListener = onEditItemClickListener;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView purposeTextView;
        TextView dateTextView;
        TextView amountTextView;
        ImageButton actionsButton;


        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setTag(this);

            purposeTextView =  itemView.findViewById(R.id.purposeTextView);
            dateTextView =  itemView.findViewById(R.id.dateTextView);
            amountTextView =  itemView.findViewById(R.id.amountTextView);
            actionsButton =  itemView.findViewById(R.id.actionsButton);



        }
    }
}

