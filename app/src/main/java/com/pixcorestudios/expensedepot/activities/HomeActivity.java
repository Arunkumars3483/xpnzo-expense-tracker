package com.pixcorestudios.expensedepot.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.util.Pair;
import androidx.lifecycle.LiveData;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DatePickerDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;

import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.pixcorestudios.expensedepot.R;
import com.pixcorestudios.expensedepot.adapters.ExpensesListAdapter;
import com.pixcorestudios.expensedepot.roomdb.models.ExpenseEntity;
import com.pixcorestudios.expensedepot.roomdb.repositories.ExpensesRepository;
import com.pixcorestudios.expensedepot.utils.Utilities;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.pixcorestudios.expensedepot.utils.Utilities.getCurrentMonthStrings;
import static com.pixcorestudios.expensedepot.utils.Utilities.getDateStringsForDateRange;


public class HomeActivity extends AppCompatActivity {

    @BindView(R.id.homeToolbar)
    Toolbar homeToolbar;

    @BindView(R.id.rupeeSymbol)
    TextView rupeeSymbol;

    @BindView(R.id.amount_text)
    TextView amountText;

    @BindView(R.id.no_expenses_text)
    TextView noExpensesText;

    @BindView(R.id.add_floating_button)
    FloatingActionButton addFloatingButton;

    @BindView(R.id.date_select_button)
    LinearLayout dateSelectButton;

    @BindView(R.id.dateTextStartContainer)
    LinearLayout dateTextStartContainer;

    @BindView(R.id.dateTextStart)
    TextView dateTextStart;

    @BindView(R.id.datesSeparatorText)
    TextView datesSeparatorText;

    @BindView(R.id.dateTextEndContainer)
    LinearLayout dateTextEndContainer;

    @BindView(R.id.dateTextEnd)
    TextView dateTextEnd;

    @BindView(R.id.mySpendsTxt)
    TextView mySpendsTxt;


    @BindView(R.id.expensesRecyclerView)
    RecyclerView expensesRecyclerView;

    ExpensesListAdapter expAdapter;
    ArrayList<ExpenseEntity> expenseEntities =  new ArrayList<ExpenseEntity>();

    LiveData<List<ExpenseEntity>> expensesLiveData;

    Date startDate, endDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        ButterKnife.bind(this);

        populateData();

    }

    private void populateData(){
        Pair<String, String> currentMonthStrings = getCurrentMonthStrings();

        dateTextStart.setText(currentMonthStrings.first);
        dateTextEnd.setText(currentMonthStrings.second);

        Pair<Date, Date> dates = Utilities.getDateRangeForThisMonth();
        startDate = dates.first;
        endDate = dates.second;


        expAdapter = new ExpensesListAdapter(expenseEntities, getApplicationContext());
        expensesRecyclerView.setHasFixedSize(true);
        expensesRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        expAdapter.setDeleteItemClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                RecyclerView.ViewHolder viewHolder = (RecyclerView.ViewHolder) view.getTag();
                int position = viewHolder.getAdapterPosition();
                ExpenseEntity delExp = expenseEntities.get(position);

                showDeleteExpensePopup(delExp);
            }
        });

        expAdapter.setOnEditItemClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                RecyclerView.ViewHolder viewHolder = (RecyclerView.ViewHolder) view.getTag();
                int position = viewHolder.getAdapterPosition();
                showEditExpensePopup(expenseEntities.get(position));
            }
        });

        expensesRecyclerView.setAdapter(expAdapter);
        refreshData();
    }

    void refreshData(){
        if(expensesLiveData != null){
            expensesLiveData.removeObservers(HomeActivity.this);
        }

        ExpensesRepository expensesRepository = new ExpensesRepository(getApplication());

        expensesLiveData = expensesRepository.getAllExpensesForDate(startDate.getTime(), endDate.getTime());
        expensesLiveData.observe(this, expenseEntities1 -> {
            expenseEntities.clear();
            expenseEntities.addAll(expenseEntities1);
            expAdapter.notifyDataSetChanged();

            if(expenseEntities1.size() > 0){
                mySpendsTxt.setVisibility(View.VISIBLE);
                expensesRecyclerView.setVisibility(View.VISIBLE);

                noExpensesText.setVisibility(View.GONE);

                long total = 0;
                for(ExpenseEntity exp: expenseEntities1){
                    total += exp.getAmount();
                }
                amountText.setText(""+total);

            }else{
                mySpendsTxt.setVisibility(View.GONE);
                expensesRecyclerView.setVisibility(View.GONE);

                noExpensesText.setVisibility(View.VISIBLE);
                amountText.setText("0");
            }
        });
    }

    @OnClick(R.id.add_floating_button)
    void addExpenseTapped(View v){
        showAddExpensePopup();
    }

    @OnClick(R.id.date_select_button)
    void dateRangeSelectTapped(View v){
        MaterialDatePicker.Builder<Pair<Long, Long>> builder = MaterialDatePicker.Builder.dateRangePicker();

        Calendar now = Calendar.getInstance();
        builder.setSelection(new Pair<Long, Long>(now.getTimeInMillis(), now.getTimeInMillis()));

        MaterialDatePicker<Pair<Long, Long>> picker = builder.build();
        picker.show(this.getSupportFragmentManager(), picker.toString());

        picker.addOnNegativeButtonClickListener(view -> {

        });
        picker.addOnPositiveButtonClickListener(selection -> {
            if(selection.first != null && selection.second !=null){
                Calendar calendar = GregorianCalendar.getInstance();
                calendar.setTimeInMillis(selection.first);
                Date dateStart = calendar.getTime();
                calendar.setTimeInMillis(selection.second);
                Date dateEnd = calendar.getTime();
                Pair<String, String> dateStrings = getDateStringsForDateRange(dateStart, dateEnd);
                dateTextStart.setText(dateStrings.first);
                dateTextEnd.setText(dateStrings.second);

                startDate = dateStart;
                endDate = dateEnd;

                refreshData();
            }else{
                Toast.makeText(getApplicationContext(), R.string.please_select_date, Toast.LENGTH_SHORT).show();
            }


        });

    }

    void showAddExpensePopup(){


        TextInputLayout amountTextInputLayout;
        TextInputEditText amountTextInput;
        TextInputLayout purposeTextInputLayout;
        TextInputEditText purposeTextInput;
        TextInputLayout selectDateTextInputLayout;
        TextInputEditText selectDateTextInput;
        AppCompatButton cancelButton;
        AppCompatButton addButton;

        Date[] selectedDate = new Date[]{new Date()};


        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setCancelable(false);

        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.popup_add_expense, null);

        dialogBuilder.setView(dialogView);
        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.getWindow().setDimAmount(0.2f);


        amountTextInputLayout =  dialogView.findViewById(R.id.amountTextInputLayout);
        amountTextInput =  dialogView.findViewById(R.id.amountTextInput);
        purposeTextInputLayout =  dialogView.findViewById(R.id.purposeTextInputLayout);
        purposeTextInput =  dialogView.findViewById(R.id.purposeTextInput);
        selectDateTextInputLayout =  dialogView.findViewById(R.id.selectDateTextInputLayout);
        selectDateTextInput =  dialogView.findViewById(R.id.selectDateTextInput);
        cancelButton =  dialogView.findViewById(R.id.cancelButton);
        addButton =  dialogView.findViewById(R.id.addButton);
        CardView closeCardBtn =  dialogView.findViewById(R.id.closeCardBtn);

        closeCardBtn.setOnClickListener(view -> alertDialog.dismiss());
        cancelButton.setOnClickListener(view -> alertDialog.dismiss());

        selectDateTextInput.setOnClickListener(view -> {
            final Calendar calendar = Calendar.getInstance();
            int day = calendar.get(Calendar.DAY_OF_MONTH);
            int month = calendar.get(Calendar.MONTH);
            int year = calendar.get(Calendar.YEAR);
            // date picker dialog
            DatePickerDialog datepicker = new DatePickerDialog(HomeActivity.this, R.style.MyDatePickerDialogTheme,
                    (sourceView, year1, monthOfYear, dayOfMonth) -> {
                        Calendar cal = Calendar.getInstance();
                        cal.setTimeInMillis(0);
                        cal.set(year1, monthOfYear, dayOfMonth);
                        Date date = cal.getTime();
                        selectedDate[0] = date;
                        String dateTxt = DateFormat.format("dd.MM.yyyy", date).toString();
                        selectDateTextInput.setText(dateTxt);

                    }, year, month, day);
            datepicker.show();
        });

        addButton.setOnClickListener(view -> {
            boolean isValid = true;
            if(amountTextInput.getText().toString().length() == 0){
                amountTextInputLayout.setErrorEnabled(true);
                amountTextInputLayout.setError(getString(R.string.required_field));
                isValid = false;
            }else{
                amountTextInputLayout.setErrorEnabled(false);
            }

            if(purposeTextInput.getText().toString().length() == 0){
                purposeTextInputLayout.setErrorEnabled(true);
                purposeTextInputLayout.setError(getString(R.string.required_field));
                isValid = false;
            }else{
                purposeTextInputLayout.setErrorEnabled(false);
            }


            if(selectDateTextInput.getText().toString().length() == 0){
                selectDateTextInputLayout.setErrorEnabled(true);
                selectDateTextInputLayout.setError(getString(R.string.required_field));
                isValid = false;
            }else{
                selectDateTextInputLayout.setErrorEnabled(false);
            }

            if(isValid){

                long amount = Long.parseLong(amountTextInput.getText().toString());
                String purpose = purposeTextInput.getText().toString();
                Date date = selectedDate[0];
                long timestamp = date.getTime();

                ExpenseEntity expenseEntity = new ExpenseEntity(purpose, timestamp, amount);

                ExpensesRepository expensesRepository = new ExpensesRepository(getApplication());
                expensesRepository.insert(expenseEntity);

                Toast.makeText(HomeActivity.this, "Expense Added", Toast.LENGTH_SHORT).show();
                alertDialog.dismiss();
            }

        });

        alertDialog.show();



    }

    void showEditExpensePopup(ExpenseEntity expenseEntity){


        TextInputLayout amountTextInputLayout;
        TextInputEditText amountTextInput;
        TextInputLayout purposeTextInputLayout;
        TextInputEditText purposeTextInput;
        TextInputLayout selectDateTextInputLayout;
        TextInputEditText selectDateTextInput;
        AppCompatButton cancelButton;
        AppCompatButton addButton;

        Date[] selectedDate = new Date[]{new Date()};


        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setCancelable(false);

        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.popup_edit_expense, null);

        dialogBuilder.setView(dialogView);
        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.getWindow().setDimAmount(0.2f);


        amountTextInputLayout =  dialogView.findViewById(R.id.amountTextInputLayout);
        amountTextInput =  dialogView.findViewById(R.id.amountTextInput);
        purposeTextInputLayout =  dialogView.findViewById(R.id.purposeTextInputLayout);
        purposeTextInput =  dialogView.findViewById(R.id.purposeTextInput);
        selectDateTextInputLayout =  dialogView.findViewById(R.id.selectDateTextInputLayout);
        selectDateTextInput =  dialogView.findViewById(R.id.selectDateTextInput);
        cancelButton =  dialogView.findViewById(R.id.cancelButton);
        addButton =  dialogView.findViewById(R.id.addButton);
        CardView closeCardBtn =  dialogView.findViewById(R.id.closeCardBtn);

        closeCardBtn.setOnClickListener(view -> alertDialog.dismiss());
        cancelButton.setOnClickListener(view -> alertDialog.dismiss());

        amountTextInput.setText(""+expenseEntity.getAmount());
        purposeTextInput.setText(expenseEntity.getPurpose());
        selectDateTextInput.setText(Utilities.getFormattedDateString(expenseEntity.getDate()));
        Calendar calen = Calendar.getInstance();
        calen.setTimeInMillis(expenseEntity.getDate());
        selectedDate[0] = calen.getTime();

        selectDateTextInput.setOnClickListener(view -> {
            final Calendar calendar = Calendar.getInstance();
            int day = calendar.get(Calendar.DAY_OF_MONTH);
            int month = calendar.get(Calendar.MONTH);
            int year = calendar.get(Calendar.YEAR);
            // date picker dialog
            DatePickerDialog datepicker = new DatePickerDialog(HomeActivity.this, R.style.MyDatePickerDialogTheme,
                    (sourceView, year1, monthOfYear, dayOfMonth) -> {
                        Calendar cal = Calendar.getInstance();
                        cal.setTimeInMillis(0);
                        cal.set(year1, monthOfYear, dayOfMonth);
                        Date date = cal.getTime();
                        selectedDate[0] = date;
                        String dateTxt = DateFormat.format("dd.MM.yyyy", date).toString();
                        selectDateTextInput.setText(dateTxt);

                    }, year, month, day);
            datepicker.show();
        });

        addButton.setOnClickListener(view -> {
            boolean isValid = true;
            if(amountTextInput.getText().toString().length() == 0){
                amountTextInputLayout.setErrorEnabled(true);
                amountTextInputLayout.setError(getString(R.string.required_field));
                isValid = false;
            }else{
                amountTextInputLayout.setErrorEnabled(false);
            }

            if(purposeTextInput.getText().toString().length() == 0){
                purposeTextInputLayout.setErrorEnabled(true);
                purposeTextInputLayout.setError(getString(R.string.required_field));
                isValid = false;
            }else{
                purposeTextInputLayout.setErrorEnabled(false);
            }


            if(selectDateTextInput.getText().toString().length() == 0){
                selectDateTextInputLayout.setErrorEnabled(true);
                selectDateTextInputLayout.setError(getString(R.string.required_field));
                isValid = false;
            }else{
                selectDateTextInputLayout.setErrorEnabled(false);
            }

            if(isValid){

                long amount = Long.parseLong(amountTextInput.getText().toString());
                String purpose = purposeTextInput.getText().toString();
                Date date = selectedDate[0];
                long timestamp = date.getTime();


                expenseEntity.setPurpose(purpose);
                expenseEntity.setDate(timestamp);
                expenseEntity.setAmount(amount);

                ExpensesRepository expensesRepository = new ExpensesRepository(getApplication());
                expensesRepository.update(expenseEntity);

                Toast.makeText(HomeActivity.this, "Expense Saved", Toast.LENGTH_SHORT).show();
                alertDialog.dismiss();
            }

        });

        alertDialog.show();



    }

    void showDeleteExpensePopup(ExpenseEntity expenseEntity){


        AppCompatButton cancelButton;
        AppCompatButton deleteButton;
        CardView closeCardBtn;

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setCancelable(false);

        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.popup_delete_expense, null);

        dialogBuilder.setView(dialogView);
        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.getWindow().setDimAmount(0.2f);



        cancelButton =  dialogView.findViewById(R.id.cancelButton);
        deleteButton =  dialogView.findViewById(R.id.deleteButton);
        closeCardBtn =  dialogView.findViewById(R.id.closeCardBtn);

        closeCardBtn.setOnClickListener(view -> alertDialog.dismiss());
        cancelButton.setOnClickListener(view -> alertDialog.dismiss());


        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ExpensesRepository expensesRepository = new ExpensesRepository(getApplication());
                expensesRepository.delete(expenseEntity.getId());
                Toast.makeText(HomeActivity.this, "Expense Deleted", Toast.LENGTH_SHORT).show();
                alertDialog.dismiss();
            }
        });

        alertDialog.show();



    }
}