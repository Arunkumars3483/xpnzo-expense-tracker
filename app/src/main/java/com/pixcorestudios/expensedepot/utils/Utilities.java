package com.pixcorestudios.expensedepot.utils;

import android.text.format.DateFormat;

import androidx.core.util.Pair;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class Utilities {

    public static Pair<Date, Date> getDateRangeForThisMonth() {
        Date begining, end;

        Calendar calendar = getCalendarForNow();
        calendar.set(Calendar.DAY_OF_MONTH,
                calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
        setTimeToBeginningOfDay(calendar);
        begining = calendar.getTime();

        calendar = getCalendarForNow();
        calendar.set(Calendar.DAY_OF_MONTH,
                calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        setTimeToEndOfDay(calendar);
        end = calendar.getTime();

        return new Pair<Date, Date>(begining, end);
    }

    public static String getFormattedDateString(long timestamp){
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(timestamp);
        Date date = cal.getTime();
        return DateFormat.format("dd.MM.yyyy", date).toString();
    }

    public static Pair<String, String> getCurrentMonthStrings(){

        Pair<Date, Date> dates = Utilities.getDateRangeForThisMonth();

        String startDate = DateFormat.format("MMM dd", dates.first).toString();
        String endDate = DateFormat.format("MMM dd", dates.second).toString();

        return new Pair<String, String>(startDate, endDate);
    }

    public static Pair<String, String> getDateStringsForDateRange(Date start, Date end){
        String startDate = DateFormat.format("MMM dd", start).toString();
        String endDate = DateFormat.format("MMM dd", end).toString();

        return new Pair<String, String>(startDate, endDate);
    }

    private static Calendar getCalendarForNow() {
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTime(new Date());
        return calendar;
    }

    private static void setTimeToBeginningOfDay(Calendar calendar) {
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
    }

    private static void setTimeToEndOfDay(Calendar calendar) {
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
    }

}
